from flask import render_template,flash,redirect,url_for,request
from app import app
from .forms import *
from models import *
@app.route('/')
@app.route('/index')
def index():
    return render_template("index.html",
                           title='Home')
